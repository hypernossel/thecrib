<!doctype html>
<html><head>
<meta charset="utf-8">
<title><?php echo SITE_TITLE ?></title>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|PT+Serif" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/hyperlogin/thecrib/master/connect_three.js"></script>
	<style>
	body
	{
		font-family: 'PT Serif', serif;
		overflow: hidden;
	}
	.wrapper
	{
		width: 100%;
		height:100%;
		margin: 0px auto;
	}
	
	#menu-categories
	{
		padding:10px;
		margin-left: -100px;
	}
	
	.overlay,.background
	{
		position:absolute;
		opacity:1;
		top:0;
		bottom:0;
		right:0;
		left:0;
		background:rgba(20,20,20,0.8);
	}
	
	.overlay
	{
		z-index:-1;
		background-color: #000000;
		background-image: url("data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h16v2h-6v6h6v8H8v-6H2v6H0V0zm4 4h2v2H4V4zm8 8h2v2h-2v-2zm-8 0h2v2H4v-2zm8-8h2v2h-2V4z' fill='%239C92AC' fill-opacity='0.2' fill-rule='evenodd'/%3E%3C/svg%3E");
	}
	
	.background
	{
		z-index: :-2;
		background: url('res/images/background.jpg') center center no-repeat,linear-gradient(to bottom right,#002f4b,#dc4225);
	  	background-size: cover;
		opacity: .6;
	}
	
	h1 { font-weight: bold; color:white;}
	h1 sub{font-size: 12px;}
	
	.header
	{
		width:30%;
		height: 50px;
		position: relative;
		margin-top: 2%;
	}
	
	#title
	{
		position: relative;
		margin-top: 10%;
	}
	
	.header > .fa:hover
	{
		color:white;
	}
	
	
	
	.large-header {
	 	position: absolute;
	 	width:100%;
	}

	form{color:black; background: white; padding: 15px; border-radius: 5px;}
	
	form p a{color:#3F3F3F; text-decoration: none;}
		
	form p a:hover{color:#FF6669;}

</style>
</head>

<body>
	<div class="wrapper">
		<div class="bg-img"></div>
		<div class="overlay"></div>
		<div id="large-header" class="large-header">
			<canvas id="demo-canvas"></canvas>
		</div>
		<div class="background"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="col-md-12 col-sm-12">
					<div id="title" class="mx-auto">
						<h1 class="text-center" >404 - Not Found</h1>
					</div>
					<br>
					<div class="col-md-6 col-sm-12 mx-auto">
						<form action="#" method="post">
						  	Opps. You have come to the Wrong Zone. Click the Button Below to Head Back.
						  	<a href="login.php" class="btn btn-info btn-block btn_back">Back</a>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="loader_bg hide">
		<div class="loader" style=""></div>
	</div>
</body>
</html>