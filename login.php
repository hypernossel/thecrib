<?php
session_start();
include('includes/site_inc.php');
if(isset($_SESSION['username'])){
	header("location:home.php");
}
?>
<!doctype html>
<html><head>
<meta charset="utf-8">
<title><?php echo SITE_TITLE ?></title>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|PT+Serif" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/hyperlogin/thecrib/master/connect_three.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/pklauzinski/jscroll/master/jquery.jscroll.js"></script>
	<style>
	body
	{
		font-family: 'PT Serif', serif;
		overflow: hidden;
	}
	.wrapper
	{
		width: 100%;
		height:100%;
		margin: 0px auto;
	}
	
	#menu-categories
	{
		padding:10px;
		margin-left: -100px;
	}
	
	.overlay,.background
	{
		position:absolute;
		opacity:1;
		top:0;
		bottom:0;
		right:0;
		left:0;
		background:rgba(20,20,20,0.8);
	}
	
	.overlay
	{
		z-index:-1;
		background-color: #000000;
		background-image: url("data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h16v2h-6v6h6v8H8v-6H2v6H0V0zm4 4h2v2H4V4zm8 8h2v2h-2v-2zm-8 0h2v2H4v-2zm8-8h2v2h-2V4z' fill='%239C92AC' fill-opacity='0.2' fill-rule='evenodd'/%3E%3C/svg%3E");
	}
	
	.background
	{
		z-index: :-2;
		background: url('res/images/background.jpg') center center no-repeat,linear-gradient(to bottom right,#002f4b,#dc4225);
	  	background-size: cover;
		opacity: .6;
	}
	
	h1 { font-weight: bold; color:white;}
	h1 sub{font-size: 12px;}
	
	.header
	{
		width:30%;
		height: 50px;
		position: relative;
		margin-top: 2%;
	}
	
	#title
	{
		position: relative;
		margin-top: 10%;
	}
	
	.header > .fa:hover
	{
		color:white;
	}
	
	.grid
	{
		position:relative;
		top:50px;
		left:12%;
		width:80%;
		
	}
	
	.grid div
	{
		padding-left:10px;
	}
	
	.grid-item,.movie_item
	{
		width: 150px;
	    height: 225px;
		border-radius: 5px;	
	}
	
	.movie_item{float:left; margin: 5px;}
	
	img {
	  display: block;
	  max-width:300px;
	  max-height:225px;
	  width: 150px;
	  height: 225px;
	}
	
	.grid-item:hover{
		border: rgba(245,245,245,1.00) 1px solid;
	}
	
	/* enable absolute positioning */
	.inner-addon {
	  position: relative;
	}

	/* style glyph */
	.inner-addon .fa {
	  position: absolute;
	  padding: 10px;
	  pointer-events: none;
	  z-index: 999;
	}
	/* align glyph */
	.left-addon .fa  { left:  0px;}
	.right-addon .fa { right: 0px;}

	/* add padding  */
	.left-addon input  { padding-left:  30px; }
	.right-addon input { padding-right: 30px; }
	

	.image,.movie_item {
	  position: relative;
	}
	
	.imageoverlay {
	  position: absolute;
	  top: 0;
	  bottom: 0;
	  left: 10px;
	  right: 0;
	  height: 100%;
	  width: auto;
	  opacity: 0;
	  transition: .5s ease;
	  background-color: rgba(0,0,0,0.6);
	  border-radius: 5px;
	} 
	
	.movie_item > .imageoverlay{ left:0px; }
	
	.image:hover .imageoverlay,.movie_item:hover .imageoverlay {
	  opacity: 1;
	}
	
	.text {
	  color: white;
	  font-size: 20px;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
		text-align: center;
	}
	.form-control-lg { width: 350px;}

	
	ul {
		position:relative;
		left:30px;
		list-style-type: none;
		margin: 0;
		padding: 0;
		width: 500px;
	} 
	li { 
		float:left;
		display:inline;
		padding:5px;
	}
	
	li > a { text-decoration: none;}
	
	.tt-menu img
	{
		width: 50px;
		height: 75px;
		margin-left:10px;
		padding: 5px;
	}
	
	.tt-menu
	{
		margin-top:15px;
		max-height: 300px;
		overflow-y: auto;
		background-color: white;
		border-radius: 3px;
	}
	
	.search_result
	{
		width: 100%;
		height: 100%;
	}
	table{
		width: 100%;
	}
	tr:hover
	{
		background:#CECECE;
	}
	tr > td {font-size: 10px; text-align: left;}
	
	#resultant{width: 100%;}
	
	.modal-lg {
		max-width: 80% !important;
		z-index: 9999;
	}
	
	.modal-header,.modal-content{background-color: #131313;color:white; border-radius: 5px 5px 0 0}
	
	.modal-content{height: 100%;}
	
	.modal-body{background-color: #131313;color:white; height:500px;max-height: calc(100vh - 143px);}
	
	.mdetails_left_info button {margin: 5px;}
	
	.mdetails_right_info,.mbdetails_right_info{max-height: 480px; overflow-y: scroll;}
	
	#cast_area {margin-left: -35px; width: 100%;}
	
	#search_box{z-index: 888; width: 100% !important;}
	
	.twitter-typeahead{
		 width: 100%;
		background-color:white;
		border-radius: 5px;
	}
	.tt-dropdown-menu{
		width: 102%;
	}
	input.typeahead.tt-query{ /* This is optional */
		width: 300px !important;
	}
	
	
	#genre_pool{margin-top:-15px;}
	
	.large-header {
	 	position: absolute;
	 	width:100%;
	}
	
	.btn_browse{z-index: 999; margin: 10px;}
	
	.mbdetails_left_info {height: 100%;}
		
	form{color:black; background: white; padding: 15px; border-radius: 5px;}
	
	form p a{color:#3F3F3F; text-decoration: none;}
		
	form p a:hover{color:#FF6669;}

</style>
<script>
	$(document).ready(function(){
		$(".btn_login").click(function(){
			$.post('login_trans.php',$("form").serializeArray(),function(result){
				console.log(result);
				if(result == "1")
					window.location = 'home.php';
				else{
					$(".alert").remove();
					$("form").after(result);
				}
			});
		});
	});
</script>
</head>

<body>
	<div class="wrapper">
		<div class="bg-img"></div>
		<div class="overlay"></div>
		<div id="large-header" class="large-header">
			<canvas id="demo-canvas"></canvas>
		</div>
		<div class="background"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div id="title" class="mx-auto">
						<h1 class="text-center">The Crib<sub>Blockbusters to the max</sub></h1>
					</div>
					<div class="col-md-3 col-sm-12 mx-auto">
						<form action="#" method="post">
						  <div class="form-group">
							<label for="tx_username">Username</label>
							<input type="text" class="form-control" id="tx_username" name="tx_username">
						  </div>
						  <div class="form-group">
							<label for="tx_password">Password</label>
							<input type="password" class="form-control" id="tx_password" name="tx_password">
						  </div>
						  <p style="font-size:12px; font-weight: bold">Need an account? <a href="#">click here</a></p>
						  <button type="button" class="btn btn-danger btn-block btn_login">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="loader_bg hide">
		<div class="loader" style=""></div>
	</div>
</body>
</html>