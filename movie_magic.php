<?php
session_start();
include( 'includes/site_inc.php' );
?>
<!doctype html>
<html>

<head>
	<meta charset="UTF-8">
	<title>
		<?php echo SITE_TITLE ?>
	</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|PT+Serif" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css" />
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/cavestri/themoviedb-javascript-library/master/themoviedb.js">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
	<style>
		body {
			background-color: black;
			font-family: 'PT Serif', serif;
			overflow: hidden;
		}
		
		.wrapper {
			width: 100%;
			height: 100%;
			margin: 0px auto;
		}
		
		#menu-categories {
			padding: 10px;
			margin-left: -100px;
		}
		
		.overlay,
		.background {
			position: absolute;
			opacity: 1;
			top: 0;
			bottom: 0;
			right: 0;
			left: 0;
			background: url(http://bit.ly/et4TJO) repeat rgba(20, 20, 20, 0.8);
		}
		
		.overlay {
			z-index: -1;
			background-color: #000000;
			background-image: url("data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h16v2h-6v6h6v8H8v-6H2v6H0V0zm4 4h2v2H4V4zm8 8h2v2h-2v-2zm-8 0h2v2H4v-2zm8-8h2v2h-2V4z' fill='%239C92AC' fill-opacity='0.2' fill-rule='evenodd'/%3E%3C/svg%3E");
		}
		
		.background {
			z-index: : -2;
			background: url('res/images/background.jpg') center center no-repeat, linear-gradient(to bottom right, #002f4b, #dc4225);
			background-size: cover;
			opacity: .6;
		}
		
		
		
		h1 {
			font-weight: bold;
			color: white;
		}
		
		h1 sub {
			font-size: 12px;
		}
		
		.header {
			width: 30%;
			height: 50px;
			position: relative;
			margin-top: 2%;
		}
		
		#title {
			position: relative;
			margin-top: 15%;
		}
		
		.header> .fa:hover {
			color: white;
		}
		
		.grid {
			position: relative;
			top: 20px;
			width: 100%;
			height: 550px;
			padding: 60px;
			overflow-y: scroll !important;
		}
		
		.grid div {
			padding-left: 10px;
		}
		
		.grid-item,
		.movie_item {
			width: 150px;
			height: 175px;
			border-radius: 5px;
		}
		
		.movie_item {
			float: left;
			margin: 5px;
		}
		
		img {
			display: block;
			max-width: 300px;
			max-height: 225px;
			width: 150px;
			height: 225px;
		}
		
		.grid-item:hover {
			border: rgba(245, 245, 245, 1.00) 1px solid;
		}
		/* enable absolute positioning */
		
		.inner-addon {
			position: relative;
		}
		/* style glyph */
		
		.inner-addon .fa {
			position: absolute;
			padding: 10px;
			pointer-events: none;
			z-index: 999;
		}
		/* align glyph */
		
		.left-addon .fa {
			left: 0px;
		}
		
		.right-addon .fa {
			right: 0px;
		}
		/* add padding  */
		
		.left-addon input {
			padding-left: 30px;
		}
		
		.right-addon input {
			padding-right: 30px;
		}
		
		.image,
		.movie_item {
			position: relative;
			top: -30px;
		}
		
		.imageoverlay {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 10px !important;
			right: 0;
			height: 100%;
			width: 100%;
			opacity: 0;
			transition: .5s ease;
			background-color: rgba(0, 0, 0, 0.6);
			border-radius: 5px;
		}
		
		.movie_item> .imageoverlay {
			left: 0px;
		}
		
		.image:hover .imageoverlay,
		.movie_item:hover .imageoverlay {
			opacity: 1;
		}
		
		.text {
			color: white;
			font-size: 12px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			text-align: center;
		}
		
		.btn_trailer {
			position:absolute;
			top:80%;
			left:55%;
			transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			text-align: center;
		}
		
		.form-control-lg {
			width: 350px;
		}
		
		table {
			width: 100%;
		}
		
		tr:hover {
			background: #CECECE;
		}
		
		tr> td {
			font-size: 10px;
			text-align: left;
		}
		
		#resultant {
			width: 100%;
		}
		
		.modal-lg {
			max-width: 80% !important;
			z-index: 9999;
		}
		
		.modal-header,
		.modal-content {
			background-color: #131313;
			color: white;
			border-radius: 5px 5px 0 0
		}
		
		.modal-content {
			height: 100%;
		}
		
		.modal-body {
			background-color: #131313;
			color: white;
			height: 500px;
			max-height: calc(100vh - 143px);
		}
		
		.mdetails_left_info button {
			margin: 5px;
		}
		
		.mdetails_right_info,
		.mbdetails_right_info {
			max-height: 480px;
			overflow-y: scroll;
		}
		
		#cast_area {
			margin-left: -35px;
			width: 100%;
		}
		
		input.typeahead.tt-query {
			/* This is optional */
			width: 300px !important;
		}
		
		#genre_pool {
			margin-top: -15px;
		}
		
		.large-header {
			position: absolute;
			width: 100%;
		}
		
		.btn_browse {
			z-index: 999;
			margin: 10px;
		}
		
		.mbdetails_left_info {
			height: 100%;
		}
		/* . LOADer ***/
		
		.loader,
		.loader_bg {
			position: absolute;
			top: 50%;
			left: 50%;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			width: 50px;
			height: 50px;
			background-color: #F46669;
			border-radius: 50%;
		}
		
		.loader_bg {
			width: 80px;
			height: 80px;
			background: linear-gradient(to bottom right, #F46669, #dc4225) !important;
			border-radius: 5px;
		}
		
		.loader:after {
			content: '';
			position: absolute;
			border-radius: 50%;
			top: 50%;
			left: 50%;
			border: 0px solid white;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			-webkit-animation: loading 1000ms ease-out forwards infinite;
			animation: loading 1000ms ease-out forwards infinite;
		}
		
		.btn_cls {
			width: 100%;
		}
		
		@-webkit-keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		@keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		.hide {
			display: none;
		}
		
		.movie-list {
			background-color: rgba(0, 0, 0, 0.5);
			height: 100%;
			width: 100%;
			color: white;
		}
	</style>
	<script>
		var tmdb_imgPath = "https://image.tmdb.org/t/p/w1280";
		var tmdb_apiKey = "43271e66afe59efbf69a9a86cc5bd466";
		var tmdb_apiPath = "https://api.themoviedb.org/3/movie/";
		
		$(document).ready(function(){
			var page = 4;
			for(var i=1; i<= 4; i++)
				upcomingmovies(i)
			
			$(".btn-load-more").click(function(){
				page = page+1;
				upcomingmovies(page);
			});
						$( ".grid").on( 'click',".movie_item > .imageoverlay" , function () {
					retrieveMovieTrailer($(this).prev().attr('data-id'));
			} );
		});
		
		function upcomingmovies(page)
		{
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			page = (page != undefined) ? page : 1;
			$.ajax( {
				url: "https://api.themoviedb.org/3/movie/upcoming?page="+page+"&api_key=" + tmdb_apiKey,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var movies = data.results;
					var mLen = movies.length;
					for ( var i = 0; i < mLen; i++ ) {
						var poster = (movies[i].poster_path == null) ? "res/images/movie_placeholder.gif" : tmdb_imgPath + movies[ i ].poster_path;
						var item = $( '<div class="movie_item"><img src="' + poster + '" class="grid-item lazy" data-image-big="' + poster + '" data-id="' + movies[ i ].id + '"/><div class="imageoverlay"><div class="text">' + movies[ i ].title + '</div></div></div>' );

						$(".grid .btn-load-more").before(item);
						
						$(".btn-load-more").fadeIn("fast");
						
					}
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} );
		}
		
		function retrieveMovieTrailer(movieid)
		{
			$.getJSON('https://api.themoviedb.org/3/movie/' + movieid + "/videos",{api_key:tmdb_apiKey},function(data){
				var lightbox = lity('//www.youtube.com/watch?v=' + data.results[0].key + "&rel=0");
			});
		}
	</script>
</head>

<body>
	<div class="wrapper">
		<div class="bg-img"></div>
		<div class="overlay"></div>
		<div id="large-header" class="large-header">
			<canvas id="demo-canvas"></canvas>
		</div>
		<div class="background"></div>
		<?php include('includes/header.php'); ?>
		<div class="container-fluid movie_latest">
			<div class="grid text-center mx-auto">
					<button class="btn btn-block btn-info btn-load-more" style="display:none;">Load More...</button>
			</div>
		</div>
	</div>

	<div class="loader_bg hide">
		<div class="loader" style=""></div>
	</div>
</body>

</html>