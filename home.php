<?php
session_start();
include( 'includes/site_inc.php' );
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title>
		<?php echo SITE_TITLE ?>
	</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|PT+Serif" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/antennaio/jquery-bar-rating/master/dist/themes/fontawesome-stars.css"/>
	<link rel="stylesheet" href="https://rawcdn.githack.com/Pixabay/jQuery-autoComplete/b9a703e62a7f9167545077627ba52b37aa800998/jquery.auto-complete.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css"/>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/twitter/typeahead.js/master/dist/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/antennaio/jquery-bar-rating/master/dist/jquery.barrating.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/cavestri/themoviedb-javascript-library/master/themoviedb.js"></script>
	<script src="js/more.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/hyperlogin/thecrib/master/connect_three.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/pklauzinski/jscroll/master/jquery.jscroll.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.min.js"></script>
	<script src="https://rawcdn.githack.com/Pixabay/jQuery-autoComplete/b9a703e62a7f9167545077627ba52b37aa800998/jquery.auto-complete.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<style>
		body {
			background-color: black;
			font-family: 'PT Serif', serif;
			overflow: hidden;
		}
		
		.wrapper {
			width: 100%;
			height: 100%;
			margin: 0px auto;
		}
		
		#menu-categories {
			padding: 10px;
			margin-left: -100px;
		}
		
		.overlay,
		.overlay_mdetails,
		.background {
			position: absolute;
			opacity: 1;
			top: 0;
			bottom: 0;
			right: 0;
			left: 0;
		}
		
		.overlay {
			z-index: -1;
			background-color: #000000;
			background-image: url("data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h16v2h-6v6h6v8H8v-6H2v6H0V0zm4 4h2v2H4V4zm8 8h2v2h-2v-2zm-8 0h2v2H4v-2zm8-8h2v2h-2V4z' fill='%239C92AC' fill-opacity='0.2' fill-rule='evenodd'/%3E%3C/svg%3E");
		}
		
		.overlay_mdetails {
			z-index: -1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		
		.background {
			z-index: : -2;
			background: url('res/images/background.jpg') center center no-repeat, linear-gradient(to bottom right, #002f4b, #dc4225);
			background-size: cover;
			opacity: .6;
		}
		
		h1 {
			font-weight: bold;
			color: white;
		}
		
		h1 sub {
			font-size: 12px;
		}
		
		.header {
			width: 30%;
			height: 50px;
			position: relative;
			margin-top: 2%;
		}
		
		#title {
			position: relative;
			margin-top: 15%;
		}
		
		.header> .fa:hover {
			color: white;
		}
		
		.grid {
			position: relative;
			top: 0px;
			width: 100%;
			height: 540px;
			padding: 30px;
			overflow-y: scroll !important;
		}
		
		.grid div {
			padding-left: 10px;
		}
		
		.grid-item,
		.movie_item {
			width: 100px;
			height: 125px;
			border-radius: 5px;
		}
		
		.recent_item {
			position: absolute;
			width: 175px;
			height: 210px;
			border-radius: 5px;
			margin: 5px 5px 5px 20px;
		}
		
		.recent_item> .imageoverlay {
			left: 0px !important;
			width: 150px;
			height: 170px;
		}
		
		.recent_item> img {
			width: 150px;
			height: 170px;
			border-radius: 5px !important;
		}
		
		.movie_item {
			float: left;
			margin: 5px;
		}
		
		img {
			display: block;
			max-width: 300px;
			max-height: 225px;
			width: 150px;
			height: 225px;
		}
		
		.grid-item:hover {
			border: rgba(245, 245, 245, 1.00) 1px solid;
		}
		
		.image,
		.movie_item {
			position: relative;
			top: -30px;
		}
		
		.imageoverlay {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 10px !important;
			right: 0;
			height: 100%;
			width: 100%;
			opacity: 0;
			transition: .5s ease;
			background-color: rgba(0, 0, 0, 0.6);
			border-radius: 5px;
		}
		
		.movie_item> .imageoverlay {
			left: 0px;
		}
		
		.recent_item> .imageoverlay {
			left: 0px;
		}
		
		.image:hover .imageoverlay,
		.movie_item:hover .imageoverlay,
		.recent_item:hover .imageoverlay {
			opacity: 1;
		}
		
		.text {
			color: white;
			font-size: 12px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			text-align: center;
		}
		
		.form-control-lg {
			width: 350px;
		}
		
		table {
			width: 100%;
		}
		
		tr:hover {
			background: #CECECE;
		}
		
		tr> td {
			font-size: 10px;
			text-align: left;
		}
		
		#resultant {
			width: 100%;
		}
		
		.modal-lg {
			max-width: 80% !important;
			z-index: 9999;
		}
		
		.modal-header,
		.modal-content {
			background-color: #131313;
			color: white;
			border-radius: 5px 5px 0 0
		}
		
		.modal-content {
			height: 100%;
		}
		
		.modal-body {
			background-color: #131313;
			color: white;
			height: 500px;
			max-height: calc(100vh - 143px);
		}
		
		.mdetails_left_info button {
			margin: 5px;
		}
		
		.mdetails_right_info,
		.mbdetails_right_info {
			max-height: 100vh;
			overflow-y: scroll;
		}
		
		#cast_area {
			margin-left: -35px;
			width: 100%;
		}
		
		input.typeahead.tt-query {
			/* This is optional */
			width: 300px !important;
		}
		
		#genre_pool {
			margin-top: -15px;
		}
		
		.large-header {
			position: absolute;
			width: 100%;
		}
		
		.btn_browse {
			z-index: 999;
			margin: 10px;
		}
		
		.mbdetails_left_info {
			height: 100vh;
		}
		/* . LOADer ***/
		
		.loader,
		.loader_bg {
			position: absolute;
			top: 50%;
			left: 50%;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			width: 50px;
			height: 50px;
			background-color: #F46669;
			border-radius: 50%;
		}
		
		.loader_bg {
			width: 80px;
			height: 80px;
			background: linear-gradient(to bottom right, #F46669, #dc4225) !important;
			border-radius: 5px;
		}
		
		.loader:after {
			content: '';
			position: absolute;
			border-radius: 50%;
			top: 50%;
			left: 50%;
			border: 0px solid white;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			-webkit-animation: loading 1000ms ease-out forwards infinite;
			animation: loading 1000ms ease-out forwards infinite;
		}
		
		.btn_cls {
			width: 100%;
		}
		
		@-webkit-keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		@keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		.hide {
			display: none;
		}
		
		.movie-list {
			background-color: rgba(0, 0, 0, 0.5);
			height: 100vh;
			width: 100%;
			color: white;
		}
		
		.movie-list> .col-sm-12 {
			height: 100vh;
		}
		
		#cast_area li {
			float: left;
			display: inline;
			padding: 5px;
		}
		
		#cast_area li> a {
			text-decoration: none;
		}
		
		#ytplayer {
			margin-top: 10px;
		}
		
		.mdetails_trailer {
			width: 100%;
			height: 100%;
			background-color: black;
		}
		
		.ico {
			position: relative;
			padding: 20px 10px 10px 10px;
			top: 10px;
		}
		
		#nav_selections> .nav-link:not(.active) {
			background-color: #434343;
			color: white;
		}
		
		#genre_area> .badge:hover {
			background-color: #2789C1;
		}
		
		.morecontent span {
			display: none;
		}
		
		.morelink {
			display: block;
		}
		
		.more {
			overflow-y: scroll !important;
			min-height: 4em !important;
		}
		
		.card,
		.card-block {
			max-height: 150px;
		}
		
		.w-100 {
			max-height: 100% !important;
		}
		
		.card {
			margin-bottom: 5px;
		}
		
		#episode_area {
			max-height: 460px !important;
			overflow-y: scroll;
		}
	</style>
	<script>
		var page = 2;
		var genre = 'default';
		var tmdb_imgPath = "https://image.tmdb.org/t/p/w1280";
		var tmdb_apiKey = "43271e66afe59efbf69a9a86cc5bd466";
		var tmdb_apiPath = "https://api.themoviedb.org/3/movie/";
		$( document ).ready( function () {
			var options = {
				loadingHtml: '<img src="loading.gif" alt="Loading" /> Loading...',
				padding: 20,
				nextSelector: '.jscroll-next',
				contentSelector: '.movie_item',
				loadingFunction: function () {
					page = page++;
					browsemovie( page, genre );
				}
			};
			$( '.grid' ).jscroll( options );

			$( '#tv_series .btn-load-more' ).on( 'click', function () {
				page = page += 1;
				browseDrama( $( this ).attr( 'next' ), genre );
			} );

			$( ".cat_movie #genre_area .badge" ).click( function () {
				$( "#genre_area .active" ).removeClass( "active" ).removeClass( "badge-danger" ).addClass( "badge-info" );
				$( this ).removeClass( "badge-info" ).addClass( "badge-danger active" );
				$( ".movie_item" ).remove();
				$( ".btn-load-more" ).fadeOut( "fast" );
				//Preload 2 Pages
				genre = $( this ).text().toLowerCase();
				browsemovie( 1, genre );
				browsemovie( 2, genre );
			} );

			$( ".cat_tv #genre_area .badge" ).click( function () {
				$( ".cat_tv #genre_area .active" ).removeClass( "active" ).removeClass( "badge-danger" ).addClass( "badge-info" );
				$( this ).removeClass( "badge-info" ).addClass( "badge-danger active" );
				$( "#tv_series .grid .movie_item" ).remove();
				$( ".cat_tv .btn-load-more" ).fadeOut( "fast" );
				//Preload 2 Pages
				genre = $( this ).attr( 'data-id' );
				browseDrama( 0, genre );
			} );


			$( '#bar_rating' ).barrating( {
				theme: 'fontawesome-stars'
			} );

			$( '.lazy' ).Lazy( {
				// your configuration goes here
				scrollDirection: 'vertical',
				effect: 'fadeIn',
				visibleOnly: true,
				onError: function ( element ) {
					console.log( 'error loading ' + element.data( 'src' ) );
				}
			} );

			$( ".btn_trailer" ).click( function () {
				var code = ( $( this ).attr( "yt_cd" ) != undefined ) ? $( this ).attr( 'yt_cd' ) : undefined;
				if ( code != undefined ) {
					var lightbox = lity( '//www.youtube.com/watch?v=' + code + "&rel=0" );
					return;
				} else {
					alert( "No Trailer has been found for selection." );
				}
			} );
			$( "#nav-movie-tab" ).click( function () {} );
			$( "#nav-tv-tab" ).click( function () {
				browseDrama( 0, 9 );
			} );

			$( ".movie_latest_add,.tv_latest_add" ).slick( {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				speed: 500,
				arrows: false,
				infinite: true,
				fade: true,
				cssEase: 'linear'
			} );

			$( ".btn_episode" ).on( 'click', function () {
				retrieveEpisodes( $( "#add_inputs" ).attr( "tv_id" ), tmdb_apiKey, 1 );
				$( ".details" ).fadeOut( "fast" );
				$( ".episodes" ).fadeIn( "fast" );
			} );
		} );

		function retrieveMovie( id ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			$.ajax( {
				url: 'getData.php?url=https://yts.am/api/v2/movie_details.json?movie_id=' + id,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var mdetail = jQuery.parseJSON( data );
					mdetail = mdetail.data.movie;
					var title = mdetail.title;
					var imdb = mdetail.imdb_code;
					var altTitle = mdetail.title_english;
					var year = mdetail.year;
					var rating = mdetail.rating;
					var runtime = mdetail.runtime;
					var intro = mdetail.description_intro;
					var trailer = mdetail.yt_trailer_code;
					var cover = mdetail.medium_cover_image;
					var torrents = mdetail.torrents;

					for ( var i = 0; i < torrents.length; i++ ) {
						if ( torrents[ i ].quality == "1080p" )
							$( ".cp_torrent" ).attr( "data-clipboard-text", torrents[ i ].url );
					}

					$( "#myModal .modal-title" ).text( "You are currently Viewing : " + title );
					$( ".title" ).text( title );
					$( ".title sub" ).text( altTitle );
					$( "#sypnosis_intro" ).text( intro );

					$( ".mdetail_cover" ).attr( "src", cover );
					getAdvanceMovieDetail( imdb );
					$( "#myModal" ).modal();
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} )
		}

		function browseDrama( page, genre ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			$( ".cat_movie" ).fadeOut( "fast" );
			$( ".cat_tv" ).fadeIn( "fast" );
			var _url = "";
			if ( page != 0 ) {
				_url = 'tools/data/drama.php?next=' + page;
			} else {
				$( ".tv_latest_add .recent_item" ).remove();
				$( "#tv_series .btn-load-more" ).removeAttr( 'disabled' );
				if ( genre == 9 )
					_url = 'tools/data/drama.php';
				else
					_url = 'tools/data/drama.php?type=' + genre;
			}

			$.ajax( {
				url: _url,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var drama = data.data;
					var mLen = drama.length;

					for ( var i = 0; i < mLen; i++ ) {

						var poster = drama[ i ].poster;

						var detail_title = drama[ i ].title[ 0 ].split( '-' )[ 0 ];
						detail_title = detail_title.replace( /[^\w\s]/gi, '' ).replace( /[0-9]/gi, '' ).replace( 'Season', '' );

						var item = $( '<div class="movie_item"><img src="' + poster + '" class="grid-item lazy" data-image-big="' + poster + '" data-link="' + drama[ i ].drama + '" data-title="' + detail_title + '"/><div class="imageoverlay"><div class="text">' + drama[ i ].title[ 0 ] + '</div></div></div>' );

						$( "#tv_series .grid .btn-load-more" ).before( item );
						if ( page == 0 ) {
							var recent = '<div class="recent_item"><img src="' + poster + '" class="grid-item lazy"/><div class="imageoverlay"><div class="text">' + drama[ i ].title[ 0 ] + '</div></div></div>';
							$( ".tv_latest_add" ).slick( 'slickAdd', recent );
						}
					}
					$( "#tv_series .btn-load-more" ).attr( 'next', data.next );
					if ( data.next == undefined )
						$( "#tv_series .btn-load-more" ).attr( 'disabled', 'disabled' );
					$( "#tv_series .btn-load-more" ).fadeIn( "fast" );
					$( ".movie_item > .imageoverlay" ).on( 'click', function () {
						getAdvanceTVDetail( $( this ).prev().attr( "data-title" ), genre );
						$( ".btn_episode" ).attr( "data-link", $( this ).prev().attr( "data-link" ) );
						$( "#modalListing" ).modal( "hide" );
						$( " #myModal" ).on( 'hidden.bs.modal', function () {
							$( ".details" ).show();
							$( ".episodes" ).hide();
						} );
						$( "#myModal" ).find( ".btn_close" ).attr( "data-browsing", true );
					} );
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} );
		}

		function browsemovie( page, genre ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			var _url = "";
			if ( genre != 'default' )
				_url = 'getData.php?url=https://yts.am/api/v2/list_movies.json?page=' + page + '&genre=' + genre + '&sort_by=year&order_by=desc';
			else
				_url = 'getData.php?url=https://yts.am/api/v2/list_movies.json?page=' + page + '&sort_by=year&order_by=asc';

			$.ajax( {
				url: _url,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					data = jQuery.parseJSON( data );
					var movies = data.data.movies;
					var mLen = movies.length;

					for ( var i = 0; i < mLen; i++ ) {

						var poster = ( movies[ i ].medium_cover_image == undefined ) ? "res/images/movie_placeholder.gif" : movies[ i ].medium_cover_image;

						var item = $( '<div class="movie_item"><img src="' + poster + '" class="grid-item lazy" data-image-big="' + poster + '" data-id="' + movies[ i ].id + '"/><div class="imageoverlay"><div class="text">' + movies[ i ].title_english + '</div></div></div>' );

						$( ".grid .btn-load-more" ).before( item );

						$( ".btn-load-more" ).fadeIn( "fast" );

					}
					$( ".movie_item > .imageoverlay" ).on( 'click', function () {
						retrieveMovie( $( this ).parent().find( "img" ).attr( "data-id" ) );
						$( "#modalListing" ).modal( "hide" );
						$( "#myModal" ).find( ".btn_close" ).attr( "data-browsing", true );
					} );
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} );
			$( ".jscroll-next" ).removeClass( 'jscroll-next' );
			$( ".movie_item:last" ).addClass( 'jscroll-next' );
		}

		function getAdvanceTVDetail( title, c_code ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );

			$.ajax( {
				url: "https://api.themoviedb.org/3/search/tv?api_key=" + tmdb_apiKey + "&query=" + title,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					console.log( data );
					for ( var i = 0; i < data.results.length; i++ ) {
						console.log( data.results[ i ].origin_country[ 0 ] + " : " + countryCode( c_code ) )
						if ( data.results[ i ].origin_country[ 0 ] == countryCode( c_code ) ) {

							data = data.results[ i ];
							break;
						}
					}

					$( "#genre_pool" ).empty();

					$( "#myModal .modal-body" ).css( {
						"background": "url('" + tmdb_imgPath + data.backdrop_path + "') center center no-repeat,linear-gradient(to bottom right,#002f4b,#dc4225)",
						"background-size": "cover",
						"opacity": "0.8"
					} );

					$( "#myModal .modal-title" ).html( "You are currently Viewing : " + data.name + " - " + data.original_name );
					$( ".title" ).text( data.name );
					$( ".title sub" ).text( data.original_name );
					$( "#sypnosis_intro" ).text( data.overview );

					$( ".mdetail_cover" ).attr( "src", tmdb_imgPath + data.poster_path );
					$( "#add_inputs" ).attr( "tv_id", data.id );
					retrieveGuestStar( data.id, tmdb_apiKey );

					$( "#myModal" ).modal();
				}
			} );

			/*
			$.ajax( {
				url: "https://api.themoviedb.org/3/movie/" + imdb + "/trailers?api_key=" + tmdb_apiKey,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var code = ( data.youtube[ 0 ].source != null ) ? data.youtube[ 0 ].source : undefined;
					if ( code != undefined )
						$( ".btn_trailer" ).attr( "yt_cd", code );
				}
			} );*/
		}

		function countryCode( code ) {
			var c_code = '';
			switch ( parseInt( code ) ) {
				case 1:
					c_code = "HK";
					break;
				case 2:
					c_code = 'US';
					break;
				case 3:
					c_code = "KR";
					break;
				case 4:
					c_code = "CN";
					break;
				case 5:
					c_code = "TW";
					break;
				case 6:
					c_code = "JP";
					break;
			}
			return c_code;
		}

		function displayEpisodes( episodes, latest, key, id ) {
			latest--;
			$.ajax( {
				url: "https://api.themoviedb.org/3/tv/" + id + "/season/1?api_key=" + key + "&append_to_response=credits",
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					for ( var i = latest; i >= 0; i-- ) {
						console.log(data.episodes[i]);
						var poster = ( data.episodes[ i ].still_path == null ) ? "res/images/image_placeholder.png" : tmdb_imgPath + data.episodes[ i ].still_path;
						var template = '<div class="card text-white bg-dark"><div class="row "><div class="col-md-4 col-xs-4"><img src="' + poster + '" class="w-100"></div><div class="col-md-8 col-xs-8 px-3"><div class="card-block px-3"><h4 class="card-title">' + episodes[ latest - i ].title[ 0 ] + '</h4><button class="btn btn-primary btn_watch" data-embed="' + episodes[ latest - i ].drama + '">Watch Now</button></div></div></div></div>';
						$( "#episode_area" ).append( template );
					}
					$(".btn_watch").on('click',function(){
							videoParser($(this).attr("data-embed"));
					});
				}
			} );
		}

		function retrieveEpisodes( id, key, season ) {
			$( "#episode_area .card" ).remove();
			//Retrieving Episode Link
			$.ajax( {
				url: "tools/data/drama.php?next=" + $( ".btn_episode" ).attr( "data-link" ),
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					displayEpisodes( data.data, data.data.length, key, id );
				}
			} )
			$( ".loader_bg" ).removeClass( "hide" ).fadeOut( "slow" );
		}

		function videoParser( link ) {
			var _url = undefined;
			if (!decodeURIComponent(link).replace( /\+/g, " " ).includes( 'v.allrss.se' ) && 
				!decodeURIComponent(link).replace( /\+/g, " " ).includes( 'vlist.se' ))
				_url = "tools/data/drama.php?next=" + link;
			else
				_url = "tools/data/drama.php?mirror=" + link.replace("%26xml%3D1","");
			
			var view_height = ($(window).height() /2) + 150;
			var view_width = ($(window).width() / 2) + 150;
			
			var posx = ($(window).width() / 2 - ((view_width/ 2) - 150/2));
			var posy = ($(window).height() / 2 - 150 / 2);
			
			$.ajax( {
				url: _url,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var win = undefined;
					if ( !decodeURIComponent(link).replace( /\+/g, " " ).includes( 'v.allrss.se' ) ) {
						console.log(data.data[0].drama.replace("&xml=1",""));
						$.ajax( {
							url: 'tools/data/drama.php?mirror=' + data.data[0].drama.replace("%26xml%3D1",""),
							type: "GET",
							contentType: 'json',
							success: function ( data ) {
								win = window.open( data.mirror, '_blank', 'location=yes,height=' + view_height + ',width=' + view_width + ',top='+posy+',left='+posx+'menubar=no,toolbar=no,scrollbars=yes,status=no' )
							}
						} )
					} else
						win = window.open( data.mirror, '_blank', 'location=yes,height=' + view_height + ',width=' + view_width + ',top='+posy+',left='+posx+'menubar=no,toolbar=no,scrollbars=yes,status=no' )
					
					var timer = setInterval(function() { 
					if(win.closed) {
						clearInterval(timer);
						alert('closed');
					}
				}, 1000);
				}
			} )
		}

		function retrieveGuestStar( id, key ) {
			//Retrieve Guest Star
			$.ajax( {
				url: "https://api.themoviedb.org/3/tv/" + id + "?api_key=" + key + "&append_to_response=credits",
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					// Filling Genre
					$( "#genre_pool" ).empty();
					for ( var i = 0; i < data.genres.length; i++ )
						$( "#genre_pool" ).append( '<span class="badge badge-pill badge-info">' + data.genres[ i ].name + '</span>&nbsp;' );
					//Filling Cast
					$( "#cast_area" ).empty();
					console.log( data );
					var casts = data.credits.cast;
					for ( var i = 0; i < ( ( casts.length > 5 ) ? 5 : casts.length ); i++ ) {
						if ( casts[ i ].profile_path == null || casts[ i ].profile_path == undefined )
							$( "#cast_area" ).append( '<li><img src="http://s3.amazonaws.com/37assets/svn/765-default-avatar.png" style="width:100px; height:150px; border-radius:3px;" /></li>' );
						else
							$( "#cast_area" ).append( '<li><img src="' + tmdb_imgPath + casts[ i ].profile_path + '" style="width:100px; height:150px; border-radius:3px;" /></li>' );
					}
					$( "#seasons span" ).text( data.number_of_seasons );
					$( "#episodes" ).text( data.number_of_episodes );
					reduceTextContent();
				}
			} );
			$( ".loader_bg" ).removeClass( "hide" ).fadeOut( "slow" );
		}
	</script>
</head>

<body>
	<div class="wrapper">
		<div class="bg-img"></div>
		<div class="overlay"></div>
		<div id="large-header" class="large-header">
			<canvas id="demo-canvas"></canvas>
		</div>
		<div class="background"></div>
		<?php include('includes/header.php'); ?>
		<div class="container-fluid movie-list">
			<div class="row">
				<div class="col-xs-12 col-md-2" style="background-color: rgba(0,0,0,0.8);">
					<div class="mbdetails_left_info mx-auto text-center cat_movie">
						<h4 style="padding: 5px 5px 5px 0; text-align: left; font-size: 18px;">Recently Added</h4>
						<div class="movie_latest_add">

						</div>
						<h4 style="text-align: left; font-size: 18px;">Categories</h4>
						<div id="genre_area">
							<span class="badge badge-pill badge-danger active">Default</span>
							<span class="badge badge-pill badge-info">Action</span>
							<span class="badge badge-pill badge-info">Animation</span>
							<span class="badge badge-pill badge-info">Adventure</span>
							<span class="badge badge-pill badge-info">Comedy</span>
							<span class="badge badge-pill badge-info">Family</span>
							<span class="badge badge-pill badge-info">Fantasy</span>
							<span class="badge badge-pill badge-info">Horror</span>
							<span class="badge badge-pill badge-info">Romance</span>
							<span class="badge badge-pill badge-info">Sci-fi</span>
							<span class="badge badge-pill badge-info">Triller</span>
						</div><br>
						<h4 style="text-align: left; font-size: 18px;">Search</h4>
						<div id="search">
							<input type="text" id="tx_search_movie" name="tx_search_movie" class="form-control"/>
							<button class="btn btn-danger btn-block btn_search_movie" style="margin-top:10px;">Search</button>
						</div>
						<br>
					</div>
					<div class="mbdetails_left_info mx-auto text-center cat_tv" style="display: none">
						<h4 style="padding: 5px 5px 5px 0; padding-top:16px; font-size: 18px; text-align: left;">Recently Updated</h4><br>
						<div class="tv_latest_add">

						</div>
						<h4 style="text-align: left; font-size: 18px;">Categories</h4>
						<div id="genre_area">
							<span class="badge badge-pill badge-danger active" data-id='9'>Recently</span>
							<span class="badge badge-pill badge-info" data-id='1'>HK TV-Series</span>
							<span class="badge badge-pill badge-info" data-id='2'>US TV-Series</span>

						

							<span class="badge badge-pill badge-info" data-id='3'>Korean Drama</span>
							<span class="badge badge-pill badge-info" data-id='4'>Chinese Drama</span>
							<span class="badge badge-pill badge-info" data-id='5'>Taiwan Drama</span>
							<span class="badge badge-pill badge-info" data-id='6'>Anime</span>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-10 grid-container">
					<nav>
						<div class="nav nav-tabs justify-content-end" id="nav_selections" role="tablist">
							<a class="nav-item nav-link active " id="nav-movie-tab" data-toggle="tab" href="#movies" role="tab" aria-controls="movies" aria-selected="true">Movies</a>
							<a class="nav-item nav-link" id="nav-tv-tab" data-toggle="tab" href="#tv_series" role="tab" aria-controls="tv_series" aria-selected="false">TV-Series</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="movies" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="grid text-center">
								<button class="btn btn-block btn-info btn-load-more" style="display:none;">Load More...</button>
							</div>
						</div>
						<div class="tab-pane fade" id="tv_series" role="tabpanel" aria-labelledby="nav-profile-tab">
							<div class="grid text-center">
								<button class="btn btn-block btn-info btn-load-more" style="display:none;">Load More...</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal for Viewing ShortCut of Movie Selection -->
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Modal Heading</h4>
					<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times" style="color:white;"></i></button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<div class="overlay_mdetails"></div>
					<div class="fluid-container mdetails_container">
						<input type="hidden" id="add_inputs"/>
						<div class="row">
							<div class="col col-3" style="border-right: white 1px solid;">
								<div class="mdetails_left_info mx-auto text-center">
									<img src="" class="mdetail_cover mx-auto"/><br>
									<button type="button" class="btn btn-warning btn_episode btn_cls"><i class="fas fa-copy "></i>&nbsp;Episodes</button><br>
									<button type="button" class="btn btn-danger btn_cls"><i class="far fa-plus-square"></i>&nbsp;Add Favourites</button><br>
									<select id="bar_rating">
										<!-- now hidden -->
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</div>
							</div>
							<div class="col col-9">
								<div class="mdetails_right_info details">
									<h4 class="title">Title HERE<sub>alt title</sub></h4>
									<p>Genre</p>
									<div id="genre_pool"></div>
									<p id="seasons">Seasons : <span class="badge badge-info"></span> Episodes : <span id="episodes" class="badge badge-info"></span>
									</p>
									<h4 class="sypnosis">Sypnosis</h4>
									<div id='sypnosis_intro' class="more"></div>
									<h4 class="casts">Casts</h4>
									<ul id="cast_area">
									</ul>
								</div>
								<div class="mdetails_right_info episodes hide">
									<div id="episode_area" class="container-fluid">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
	<div class="loader_bg hide">
		<div class="loader" style=""></div>
	</div>
</body>

</html>