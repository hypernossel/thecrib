<?php
session_start();
include( 'includes/site_inc.php' );
if ( $_SESSION[ 'flags' ] != 1 )
	header( '404.php' );
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title>
		<?php echo SITE_TITLE ?>
	</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|PT+Serif" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/antennaio/jquery-bar-rating/master/dist/themes/fontawesome-stars.css"/>
	<link rel="stylesheet" href="https://rawcdn.githack.com/Pixabay/jQuery-autoComplete/b9a703e62a7f9167545077627ba52b37aa800998/jquery.auto-complete.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css"/>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/twitter/typeahead.js/master/dist/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/antennaio/jquery-bar-rating/master/dist/jquery.barrating.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/cavestri/themoviedb-javascript-library/master/themoviedb.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/jedfoster/Readmore.js/master/readmore.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/hyperlogin/thecrib/master/connect_three.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/pklauzinski/jscroll/master/jquery.jscroll.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.min.js"></script>
	<script src="https://rawcdn.githack.com/Pixabay/jQuery-autoComplete/b9a703e62a7f9167545077627ba52b37aa800998/jquery.auto-complete.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
	<style>
		body {
			background-color: black;
			font-family: 'PT Serif', serif;
			overflow: hidden;
		}
		
		.wrapper {
			width: 100%;
			height: 100%;
			margin: 0px auto;
		}
		
		#menu-categories {
			padding: 10px;
			margin-left: -100px;
		}
		
		.overlay,
		.overlay_mdetails,
		.background {
			position: absolute;
			opacity: 1;
			top: 0;
			bottom: 0;
			right: 0;
			left: 0;
		}
		
		.overlay {
			z-index: -1;
			background-color: #000000;
			background-image: url("data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h16v2h-6v6h6v8H8v-6H2v6H0V0zm4 4h2v2H4V4zm8 8h2v2h-2v-2zm-8 0h2v2H4v-2zm8-8h2v2h-2V4z' fill='%239C92AC' fill-opacity='0.2' fill-rule='evenodd'/%3E%3C/svg%3E");
		}
		
		.overlay_mdetails {
			z-index: -1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		
		.background {
			z-index: : -2;
			background: url('res/images/background.jpg') center center no-repeat, linear-gradient(to bottom right, #002f4b, #dc4225);
			background-size: cover;
			opacity: .6;
		}
		
		h1 {
			font-weight: bold;
			color: white;
		}
		
		h1 sub {
			font-size: 12px;
		}
		
		.header {
			width: 30%;
			height: 50px;
			position: relative;
			margin-top: 2%;
		}
		
		#title {
			position: relative;
			margin-top: 15%;
		}
		
		.header> .fa:hover {
			color: white;
		}
		
		.grid {
			position: relative;
			top: 0px;
			width: 100%;
			height: 540px;
			padding: 30px;
			overflow-y: scroll !important;
		}
		
		.grid div {
			padding-left: 10px;
		}
		
		.grid-item,
		.movie_item {
			width: 100px;
			height: 125px;
			border-radius: 5px;
		}
		
		.movie_item {
			float: left;
			margin: 5px;
		}
		
		img {
			display: block;
			max-width: 300px;
			max-height: 225px;
			width: 150px;
			height: 225px;
		}
		
		.grid-item:hover {
			border: rgba(245, 245, 245, 1.00) 1px solid;
		}
		/* enable absolute positioning */
		
		.inner-addon {
			position: relative;
		}
		/* style glyph */
		
		.inner-addon .fa {
			position: absolute;
			padding: 10px;
			pointer-events: none;
			z-index: 999;
		}
		/* align glyph */
		
		.left-addon .fa {
			left: 0px;
		}
		
		.right-addon .fa {
			right: 0px;
		}
		/* add padding  */
		
		.left-addon input {
			padding-left: 30px;
		}
		
		.right-addon input {
			padding-right: 30px;
		}
		
		.image,
		.movie_item {
			position: relative;
			top: -30px;
		}
		
		.imageoverlay {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 10px !important;
			right: 0;
			height: 100%;
			width: 100%;
			opacity: 0;
			transition: .5s ease;
			background-color: rgba(0, 0, 0, 0.6);
			border-radius: 5px;
		}
		
		.movie_item> .imageoverlay {
			left: 0px;
		}
		
		.image:hover .imageoverlay,
		.movie_item:hover .imageoverlay {
			opacity: 1;
		}
		
		.text {
			color: white;
			font-size: 12px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			text-align: center;
		}
		
		.form-control-lg {
			width: 350px;
		}
		
		table {
			width: 100%;
		}
		
		tr:hover {
			background: #CECECE;
		}
		
		tr> td {
			font-size: 10px;
			text-align: left;
		}
		
		#resultant {
			width: 100%;
		}
		
		.modal-lg {
			max-width: 80% !important;
			z-index: 9999;
		}
		
		.modal-header,
		.modal-content {
			background-color: #131313;
			color: white;
			border-radius: 5px 5px 0 0
		}
		
		.modal-content {
			height: 100%;
		}
		
		.modal-body {
			background-color: #131313;
			color: white;
			height: 500px;
			max-height: calc(100vh - 143px);
		}
		
		.mdetails_left_info button {
			margin: 5px;
		}
		
		.mdetails_right_info,
		.mbdetails_right_info {
			max-height: 100vh;
			overflow-y: scroll;
		}
		
		#cast_area {
			margin-left: -35px;
			width: 100%;
		}
		
		input.typeahead.tt-query {
			/* This is optional */
			width: 300px !important;
		}
		
		#genre_pool {
			margin-top: -15px;
		}
		
		.large-header {
			position: absolute;
			width: 100%;
		}
		
		.btn_browse {
			z-index: 999;
			margin: 10px;
		}
		
		.mbdetails_left_info {
			height: 100vh;
		}
		/* . LOADer ***/
		
		.loader,
		.loader_bg {
			position: absolute;
			top: 50%;
			left: 50%;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			width: 50px;
			height: 50px;
			background-color: #F46669;
			border-radius: 50%;
		}
		
		.loader_bg {
			width: 80px;
			height: 80px;
			background: linear-gradient(to bottom right, #F46669, #dc4225) !important;
			border-radius: 5px;
		}
		
		.loader:after {
			content: '';
			position: absolute;
			border-radius: 50%;
			top: 50%;
			left: 50%;
			border: 0px solid white;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			-webkit-animation: loading 1000ms ease-out forwards infinite;
			animation: loading 1000ms ease-out forwards infinite;
		}
		
		.btn_cls {
			width: 100%;
		}
		
		@-webkit-keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		@keyframes loading {
			0% {
				border: 0px solid white;
			}
			20% {
				border: 8px solid white;
				width: 0%;
				height: 0%;
			}
			100% {
				border: 8px solid white;
				width: 100%;
				height: 100%;
			}
		}
		
		.hide {
			display: none;
		}
		
		.movie-list {
			background-color: rgba(0, 0, 0, 0.5);
			height: 100vh;
			width: 100%;
			color: white;
		}
		
		.movie-list> .col-sm-12 {
			height: 100vh;
		}
		
		#cast_area li {
			float: left;
			display: inline;
			padding: 5px;
		}
		
		#cast_area li> a {
			text-decoration: none;
		}
		
		#ytplayer {
			margin-top: 10px;
		}
		
		.mdetails_trailer {
			width: 100%;
			height: 100%;
			background-color: black;
		}
		
		.ico {
			position: relative;
			padding: 20px 10px 10px 10px;
			top: 10px;
		}
	</style>
	<script>
		var page = 2;
		var genre = 'default';
		$( document ).ready( function () {
			browserss();
			//setTimeout( browsemovie( 1, genre ), 1000 );
			//setTimeout( browsemovie( 2, genre ), 2000 );
			var options = {
				loadingHtml: '<img src="loading.gif" alt="Loading" /> Loading...',
				padding: 20,
				nextSelector: '.jscroll-next',
				contentSelector: '.movie_item',
				loadingFunction: function () {
					page = page++;
					browsemovie( page, genre );
				}
			};
			$( '.grid' ).jscroll( options );

			$( '.btn-load-more' ).on( 'click', function () {
				page = page += 1;
				var attr = $(this).attr('data-next');
				if(typeof attr !== typeof undefined && attr !== false)
					browserss($(this).attr('data-next'));
				else
					browsemovie( page, genre );
			} );
			$( "#genre_area .badge" ).click( function () {
				$( "#genre_area .active" ).removeClass( "active" ).removeClass( "badge-danger" ).addClass( "badge-info" );
				$( this ).removeClass( "badge-info" ).addClass( "badge-danger active" );
				$( ".movie_item" ).remove();
				$( ".btn-load-more" ).fadeOut( "fast" );
				//Preload 2 Pages
				genre = $( this ).text().toLowerCase();
				browsemovie( 1, genre );
				browsemovie( 2, genre );
			} );

			$( '#bar_rating' ).barrating( {
				theme: 'fontawesome-stars'
			} );

			var clipboard = new ClipboardJS( '.cp_torrent' );
			clipboard.on( 'success', function ( e ) {
				e.clearSelection();
			} );

			$( '.lazy' ).Lazy( {
				// your configuration goes here
				scrollDirection: 'vertical',
				effect: 'fadeIn',
				visibleOnly: true,
				onError: function ( element ) {
					console.log( 'error loading ' + element.data( 'src' ) );
				}
			} );

			$( '.cp_torrent' ).tooltip( {
				animated: 'fade',
				placement: 'top',
				trigger: 'click'
			} );
			$( '.cp_torrent' ).on( 'shown.bs.tooltip', function () {
				setTimeout( function () {
					$( '.cp_torrent' ).tooltip( 'hide' );
				}, 2000 );
			} )

			$( ".btn_trailer" ).click( function () {
				var code = ( $( this ).attr( "yt_cd" ) != undefined ) ? $( this ).attr( 'yt_cd' ) : undefined;
				if ( code != undefined ) {
					var lightbox = lity( '//www.youtube.com/watch?v=' + code + "&rel=0" );
					return;
				} else {
					alert( "No Trailer has been found for selection." );
				}

			} );

			var xhr;
			/*new autoComplete({
				minChars: 1,
				selector:'input[id="tx_search_movie"]',
				source: function(term, response){
					try { xhr.abort(); } catch(e){}
					xhr = $.getJSON('https://yts.am/api/v2/list_movies.json', { query_term: term }, function(data){ response(data);});
				},
				onSelect: function(e, term, item){
					alert('Item "'+item.data('langname')+' ('+item.data('lang')+')" selected by '+(e.type == 'keydown' ? 'pressing enter' : 'mouse click')+'.');
				}
			});*/

		} );

		function retrieveMovie( id ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			$.ajax( {
				url: 'tools/data/getData.php?url=https://yts.am/api/v2/movie_details.json?movie_id=' + id,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var mdetail = jQuery.parseJSON( data );
					mdetail = mdetail.data.movie;
					var title = mdetail.title;
					var imdb = mdetail.imdb_code;
					var altTitle = mdetail.title_english;
					var year = mdetail.year;
					var rating = mdetail.rating;
					var runtime = mdetail.runtime;
					var intro = mdetail.description_intro;
					var trailer = mdetail.yt_trailer_code;
					var cover = mdetail.medium_cover_image;
					var torrents = mdetail.torrents;

					for ( var i = 0; i < torrents.length; i++ ) {
						if ( torrents[ i ].quality == "1080p" )
							$( ".cp_torrent" ).attr( "data-clipboard-text", torrents[ i ].url );
					}

					$( "#myModal .modal-title" ).text( "You are currently Viewing : " + title );
					$( ".title" ).text( title );
					$( ".title sub" ).text( altTitle );
					$( "#sypnosis_intro" ).text( intro );

					$( ".mdetail_cover" ).attr( "src", cover );
					$( '#sypnosis_intro' ).readmore( {
						speed: 75,
						moreLink: '<a href="#">Read more</a>',
						lessLink: '<a href="#">Read less</a>'
					} );
					getAdvanceMovieDetail( imdb );
					$( "#myModal" ).modal();
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} )
		}

		function browserss(url) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			var _url = (url == undefined) ? "tools/data/drama.php?type=7" : "tools/data/drama.php?next=" + url;
			$.ajax( {
				url: _url,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var movies = data.data;
					var mLen = movies.length;

					for ( var i = 0; i < mLen; i++ ) {

						var poster = movies[ i ].poster;

						var item = $( '<div class="movie_item"><img src="' + poster + '" class="grid-item lazy" data-image-big="' + poster + '" data-link="' + movies[ i ].drama + '"/><div class="imageoverlay"><div class="text">' + movies[ i ].title[0] + '</div></div></div>' );

						$( ".grid .btn-load-more" ).before( item );
					}
					$( ".btn-load-more" ).fadeIn( "fast" );
					$(".btn-load-more").attr({
						'type':'rss',
						'data-next':(data.next == undefined) ? undefined : data.next,
					});
					$( ".movie_item > .imageoverlay" ).on( 'click', function () {
						retrieveMovie( $( this ).parent().find( "img" ).attr( "data-id" ) );
						$( "#modalListing" ).modal( "hide" );
						$( "#myModal" ).find( ".btn_close" ).attr( "data-browsing", true );
					} );
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} );
			$( ".jscroll-next" ).removeClass( 'jscroll-next' );
			$( ".movie_item:last" ).addClass( 'jscroll-next' );
		}

		function browsemovie( page, genre ) {
			$( ".loader_bg" ).removeClass( "hide" ).fadeIn( "slow" );
			var _url = "";
			if ( genre != 'default' )
				_url = 'tools/data/getData.php?url=https://yts.am/api/v2/list_movies.json?page=' + page + '&genre=' + genre + '&sort_by=year&order_by=desc';
			else
				_url = 'tools/data/getData.php?url=https://yts.am/api/v2/list_movies.json?page=' + page + '&sort_by=year&order_by=asc';

			$.ajax( {
				url: _url,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					data = jQuery.parseJSON( data );
					var movies = data.data.movies;
					var mLen = movies.length;

					for ( var i = 0; i < mLen; i++ ) {

						var poster = ( movies[ i ].medium_cover_image == undefined ) ? "res/images/movie_placeholder.gif" : movies[ i ].medium_cover_image;

						var item = $( '<div class="movie_item"><img src="' + poster + '" class="grid-item lazy" data-image-big="' + poster + '" data-id="' + movies[ i ].id + '"/><div class="imageoverlay"><div class="text">' + movies[ i ].title_english + '</div></div></div>' );

						$( ".grid .btn-load-more" ).before( item );

						$( ".btn-load-more" ).fadeIn( "fast" );

					}
					$( ".movie_item > .imageoverlay" ).on( 'click', function () {
						retrieveMovie( $( this ).parent().find( "img" ).attr( "data-id" ) );
						$( "#modalListing" ).modal( "hide" );
						$( "#myModal" ).find( ".btn_close" ).attr( "data-browsing", true );
					} );
					$( ".loader_bg" ).fadeOut( "slow" ).addClass( "hide" );
				}
			} );
			$( ".jscroll-next" ).removeClass( 'jscroll-next' );
			$( ".movie_item:last" ).addClass( 'jscroll-next' );
		}

		function getAdvanceMovieDetail( imdb ) {
			var tmdb_imgPath = "https://image.tmdb.org/t/p/w1280";
			var tmdb_apiKey = "43271e66afe59efbf69a9a86cc5bd466";
			var tmdb_apiPath = "https://api.themoviedb.org/3/movie/";

			$.ajax( {
				url: "https://api.themoviedb.org/3/movie/" + imdb + "?api_key=" + tmdb_apiKey,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					$( "#genre_pool" ).empty();
					console.log(data);
					for ( var i = 0; i < data.genres.length; i++ )
						$( "#genre_pool" ).append( '<span class="badge badge-pill badge-info">' + data.genres[ i ].name + '</span>&nbsp;' );


					$( "#myModal .modal-body" ).css( {
						"background": "url('" + tmdb_imgPath + data.backdrop_path + "') center center no-repeat,linear-gradient(to bottom right,#002f4b,#dc4225)",
						"background-size": "cover",
						"opacity": "0.8"
					} );
				}
			} );
			$.ajax( {
				url: "https://api.themoviedb.org/3/movie/" + imdb + "/credits?api_key=" + tmdb_apiKey,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					$( "#cast_area" ).empty();
					for ( var i = 0; i < 5; i++ ) {
						if ( data.cast[ i ].profile_path == null || data.cast[ i ].profile_path == undefined )
							$( "#cast_area" ).append( '<li><img src="http://s3.amazonaws.com/37assets/svn/765-default-avatar.png" style="width:100px; height:150px; border-radius:3px;" /></li>' );
						else
							$( "#cast_area" ).append( '<li><img src="' + tmdb_imgPath + data.cast[ i ].profile_path + '" style="width:100px; height:150px; border-radius:3px;" /></li>' );

					}

				}
			} );

			$.ajax( {
				url: "https://api.themoviedb.org/3/movie/" + imdb + "/trailers?api_key=" + tmdb_apiKey,
				type: "GET",
				contentType: 'json',
				success: function ( data ) {
					var code = ( data.youtube[ 0 ].source != null ) ? data.youtube[ 0 ].source : undefined;
					if ( code != undefined )
						$( ".btn_trailer" ).attr( "yt_cd", code );
				}
			} );
		}
	</script>
</head>

<body>
	<div class="wrapper">
		<div class="bg-img"></div>
		<div class="overlay"></div>
		<div id="large-header" class="large-header">
			<canvas id="demo-canvas"></canvas>
		</div>
		<div class="background"></div>
		<?php include('includes/header.php'); ?>
		<div class="container-fluid movie-list">
			<div class="row">
				<div class="col-sm-12 col-md-2" style="background-color: rgba(0,0,0,0.8);">
					<div class="mbdetails_left_info mx-auto text-center">
						<h4 style="padding: 5px;">Search</h4>
						<div id="search">
							<div class="input-group mb-3">
								<input type="text" class="form-control" id="tx_search_movie" name="tx_search_movie" placeholder="Search" aria-label="Recipient's username" aria-describedby="tx_search_movie">
								<div class="input-group-append">
									<button class="btn btn-danger" type="button"><i class="fas fa-search"></i></button>
								</div>
							</div>
						</div>
						<h4>Source</h4>
						<div id="genre_area">
							<span class="badge badge-pill badge-danger active">YTS.ag</span>
							<span class="badge badge-pill badge-info">RSS</span>

						</div><br>
						<h4>Categories</h4>
						<div id="genre_area">
							<span class="badge badge-pill badge-danger active">Default</span>
							<span class="badge badge-pill badge-info">Action</span>
							<span class="badge badge-pill badge-info">Animation</span>
							<span class="badge badge-pill badge-info">Adventure</span>
							<span class="badge badge-pill badge-info">Comedy</span>
							<span class="badge badge-pill badge-info">Family</span>
							<span class="badge badge-pill badge-info">Fantasy</span>
							<span class="badge badge-pill badge-info">Horror</span>
							<span class="badge badge-pill badge-info">Romance</span>
							<span class="badge badge-pill badge-info">Sci-fi</span>
							<span class="badge badge-pill badge-info">Triller</span>
						</div><br>
						<h4>Sorting</h4>
						<div id="sortType">
							<span class="badge badge-pill badge-warning"><i class="fas fa-sort-amount-up"></i>&nbsp;Acending</span>
							<span class="badge badge-pill badge-warning"><i class="fas fa-sort-numeric-down"></i>&nbsp;Decending</span>
						</div>
						<br>
					</div>
				</div>
				<div class="col-sm-12 col-md-10 grid-container">
					<h4 style="margin-left: 50px; padding: 5px;">Movies</h4>
					<div class="grid text-center">
						<button class="btn btn-block btn-info btn-load-more" style="display:none;">Load More...</button>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Modal for Viewing ShortCut of Movie Selection -->
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Modal Heading</h4>
					<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times" style="color:white;"></i></button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<div class="overlay_mdetails"></div>
					<div class="fluid-container mdetails_container">
						<div class="row">
							<div class="col col-3" style="border-right: white 1px solid;">
								<div class="mdetails_left_info mx-auto text-center">
									<img src="" class="mdetail_cover mx-auto"/><br>
									<button type="button" class="btn btn-info btn_trailer btn_cls"><i class="fas fa-tv"></i>&nbsp;View Trailer</button><br>
									<button type="button" class="btn btn-warning btn_cls cp_torrent" data-clipboard-text="undefined" title="Copied!"><i class="fas fa-copy "></i>&nbsp;Torrent</button><br>
									<button type="button" class="btn btn-danger btn_cls"><i class="far fa-plus-square"></i>&nbsp;Add Selection</button><br>
									<select id="bar_rating">
										<!-- now hidden -->
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</div>
							</div>
							<div class="col col-9">
								<div class="mdetails_right_info">
									<h4 class="title">Title HERE<sub>alt title</sub></h4>
									<p>Genre</p>
									<div id="genre_pool"></div>
									<h4 class="sypnosis">Sypnosis</h4>
									<div id='sypnosis_intro'></div>
									<h4 class="casts">Casts</h4>
									<ul id="cast_area">
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
	<div class="loader_bg hide">
		<div class="loader" style=""></div>
	</div>
</body>

</html>