<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		



			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand py-0" href="#">TheCrib</a>
				<div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
					<ul class="navbar-nav ml-auto" style="float:right;">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle py-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-circle"></i>&nbsp;
								<?php echo $_SESSION['username']; ?>
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="home.php"><i class="fas fa-bars"></i>&nbsp;The Crib</a>
								<a class="dropdown-item" href="movie_magic.php"><i class="fas fa-film"></i>&nbsp;Movie Magic</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="profile.php"><i class="fas fa-user"></i>&nbsp;Profile/Settings</a>
								<a class="dropdown-item" href="admin.php"><i class="fas fa-unlock-alt"></i>&nbsp;Admin</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt"></i>&nbsp;Logout</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>