<?php
	include("db_config.inc.php");

	function query($query)
		{
			$conn = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME) or die(mysqli_error());
			$result = mysqli_query($conn,$query);
			$result = mysqli_fetch_assoc($result);
			return $result;
		}
?>