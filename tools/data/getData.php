<?php
	if(!isset($_GET['url']))
		echo json_encode(array("data",array("result","failed")));
	else
	{
		if(isset($_GET['query_term']))
		{
			$search_Arry = [];
			$url = $_GET['url'].'?query_term='.urlencode($_GET['query_term']);
			$data = json_decode(file_get_contents($url),true);
			foreach($data["data"]['movies'] as $movie)
			{
				array_push($search_Arry,array("name"=>$movie['title_english'],"icon"=>$movie['medium_cover_image'],"mid"=>$movie['id']));
			}
			
			echo json_encode($search_Arry);
		}else	
			echo file_get_contents($_GET['url']);
	}
?>