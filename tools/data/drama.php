<?php
header( 'Content-Type: application/json' );
// 0 - Recently
// 1 - HK Drama
// 2 - US TV Series
// 3 - KDrama
// 4 - Chinese Drama
// 5 - Taiwanese Drama
// 6 - Newest Anime
// 7 - Movies
$url = "";
if ( isset( $_GET[ 'mirror' ] ) ) {
	preg_match('/(var)\s(VB_IC)\s\=\s\".+\"/',file_get_contents($_GET['mirror']),$matches,PREG_OFFSET_CAPTURE);
	//print_r(file_get_contents($_GET['mirror']));
	$result = (str_replace("var VB_IC =","",$matches[0][0]));
	echo json_encode(array('mirror'=> 'http://icdrama.to/vidembed.'.substr($result,2,-1).'.html'));
	
} else {
	if ( !isset( $_GET[ 'next' ] ) ) {
		switch ( $_GET[ 'type' ] ) {
			case 1:
				$url = 'http://allrss.se/dramas/?channel=hk-drama&nocache=1';
				break;
			case 2:
				$url = 'http://rss.watch-123movies.com/ustvraw.php?channel=tv-series';
				break;
			case 3:
				$url = 'http://allrss.se/dramas/?channel=korean-drama-englishsubtitles&nocache=1';
				break;
			case 4:
				$url = 'http://allrss.se/dramas/?channel=chinese-drama-englishsubtitles&nocache=1';
				break;
			case 5:
				$url = 'http://allrss.se/dramas/?channel=taiwanese-drama-englishsubtitles&nocache=1';
				break;
			case 6:
				$url = 'http://allrss.se/anime.php?channel=justupdated';
				break;
			case 7:
				$url = 'http://allrss.se/dramas/?channel=movies&nocache=1';
				break;
			default:
				$url = 'http://allrss.se/dramas/?channel=recently&nocache=1';
				break;
		}
	} else {
		$url = $_GET[ 'next' ];
	}

	$xml = simplexml_load_file( $url )or die( "Error: Cannot create object" );

	$items = $xml->xpath( "channel/item" );
	$size = sizeof( $xml->xpath( 'channel/item' ) );
	$isDrama = $_GET[ 'type' ] == 'drama' && isset( $_GET[ 'next' ] );
	$itemsArry = array();
	$itemsData = array();
	for ( $i = 0; $i < ( ( $isDrama || !( strpos( $items[ $size - 1 ]->children()[ 0 ], 'Page' ) !== false ) ) ? $size : $size - 1 ); $i++ ) {
		$title = '';
		$listItem = array();
		foreach ( $items[ $i ]->children() as $child ) {

			if ( $child->getName() == 'title' )
				$listItem[ 'title' ] = $child;
			if ( $child->getName() == 'enclosure' ) {
				if ( $_GET[ 'type' ] == "drama" ) {
					$listItem[ 'episode_link' ] = str_replace( "xml=1", "", $child->attributes()[ 0 ] );
				} else
					$listItem[ 'drama' ] = urlencode( str_replace("xml=1","",$child->attributes()[ 0 ] ));
				//echo "<br><a href='xml_test.php?type=drama&next=""'>$title</a>";
			}
			if ( $child->getName() == 'description' ) {
				$doc = new DOMDocument();
				$doc->loadHTML( str_replace( "w=150&h=84", "w=175&h=220", $child ) );
				$poster = $doc->getElementsByTagName( 'img' )[ 0 ]->getAttribute( 'src' );
				$listItem[ 'poster' ] = $poster;
			}
		}
		array_push( $itemsData, $listItem );
	}
	$itemsArry[ 'data' ] = $itemsData;
	if ( !$isDrama && strpos( $items[ $size - 1 ]->children()[ 0 ], 'Page' ) !== false ) {
		foreach ( $items[ $size - 1 ]->children() as $child ) {
			if ( $child->getName() == 'enclosure' )
				$itemsArry[ 'next' ] = urlencode( $child[ 1 ]->attributes()[ 0 ] );
		}
	}
	echo json_encode( $itemsArry );
}

function findVar($var, $url){
    $data = file_get_contents($url);
    if(strpos($data, $var) == false) return false;
    $len = strlen($var) + 2;
    $start = strpos($data, $var."='") + $len; 
    $stop = strpos($data, "'", $start); 
    $val = substr($data, $start, ($stop-$start));
    return $val; 
}
?>